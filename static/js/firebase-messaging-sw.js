// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');
importScripts('firebase-init.js');

var messaging = firebase.messaging();

/**
 * Here is is the code snippet to initialize Firebase Messaging in the Service
 * Worker when your app is not hosted on Firebase Hosting.

 // [START initialize_firebase_in_sw]
 // Give the service worker access to Firebase Messaging.
 // Note that you can only use Firebase Messaging here, other Firebase libraries
 // are not available in the service worker.
 importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
 importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

 // Initialize the Firebase app in the service worker by passing in the
 // messagingSenderId.
 firebase.initializeApp({
   'messagingSenderId': 'YOUR-SENDER-ID'
 });

 // Retrieve an instance of Firebase Messaging so that it can handle background
 // messages.
 const messaging = firebase.messaging();
 // [END initialize_firebase_in_sw]
 **/
let baseUrl = null;
self.addEventListener('install', event => {
  baseUrl = new URL(location).searchParams.get('baseUrl');
});

// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function(payload) {
  return self.registration.showNotification(payload.data.title, {
    title: payload.data.title,
    body: payload.data.body,
    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAc0UlEQVR42u2df3Bc1Z3lP+epUTTCSC3Fcbm8jItiXGBLCmEdimIZiiWsl5BsJr8hEJLZZIAQSBgqyXgJltoIJMXrojKMx0MSyDi/WBhCGJYhCUtYls3MpBKvCwgxkmxcXspFubwux1iSozhCbr2zf7R+tGzZei11S63WPX/Yx2C3Xt973r3nnXvf94qAkqK/64JaxSMtFmsN/xZoEpwL1IFrQP02h4X3GL0ieAmxM93Wsy+0XuFQaIISiLjj/JSJzkbRR4GPyKxCLLWdknJNbpupOPYg0kGbF8A/RHq5oa2nP7RqEPS8oK+zeans2ww3AOdJwgbIibZAfszwc0GHs2fsaGh/JQ4tHAQ9N0K+t7kG8VHBfYgVo5pEgtlymWGLR7E7GjK9r4fWDoIurZg7mlaAWsF/DiwRo6IcbeBicCBG9Mq6xWhHQ6Y7G1r+ZEShCWYr5pZGSY9IfF7KiRlyo2sxuSAStAA/An84tHwQdAke/ppXi/hn2FcYR2MytCYmv+JzrwC29Xc2fy70QLAcRRRz09nANsNV89Rz/cAnIvx8fduu8LAYRujZwdImpHV5loA55ZAWbLG1KvRGGKFn7pk710Q4ugnx4FjjOa8h55pjP4O4vqGt92jonTBCz2BojlYjf0UwmhePOtt54ogrgOtCxwRBF4y4HYBbhc4bSyDISyPmhaNaWXf9vmtNTeihIOiCcDTVdDHyx8en+/ypfz65OGc4jm7qa78wCoIOSO424BrMsvm2Gafgn1B0fGkQdEAiHLzvgpTgOuW1WVlYjgl+EZHPC4IOSISat0YuB509pqCyshyARY3Nh4KgA6bFm5uaI8N7xnbBlanlQNIlv+9sqQ6CDjgtNOJa4AKdsFuozCwHwIrj8oog6IBpGkl1gpX5Cio7y5H7pY6Ys4OgA6bLN5aAl+ULSHmKKhsuahFLg6ADpjMd1YbaSbO7nbcGXR7cOGVcu5h7KhXEmuzGF6qZ0HLudRIZTBnxXH9WB0EHTIcsMGRTM/ndqNHJvlw4ziINBcsRMJ2HHgYGJVBuJER58V0Z8axNEHTANHI2g7YP5FsOj47SZcWlQYmDQdABp38khKOC/faEfx6f7suI2z5qxweCoANOi7oRHQN+swAsx/7sSBB0wHSN1N4DkZ4pf8vB/3pH+2vZIOiAJLndTvDeMrYcg4LnF30/BakmtB0beoaxHi9DmzHGX8TsCYIOSJ52KP4+4vVytBxID6QzvUeCoAMSo1rslXkCEZeT5ZDotvST0ENB0AXhzNZdMfBtoT1lZDkGgc0Nrd1DoYeCoAtGOtO7l5j1SNlysBw2j0eqfiz0TBD0jDESVz0L/pZheD4th/B28Ka61ldCJdIg6Jnj7e07s3Z0D/iZebQc/aDb0++o2Rt6ZAKhFNgs0N/Vsow4fsDSx+dymyjmDeDGhkzP86EXwghdNNQfX3oIuBV4As2R/cC9gpuFXgg9EEbo0ozUnS1pO14v6cvYNUwcAESR+cvGn86eMdL7jjtfCw0fBF06vNm+Koqqqi8G3SdxCZDK6TA3vM6Ggw+C7nfsbzRs7B0MrR0EPXejdUfTMqMPIm4WXODcWYSFf5CJc0LmBaGvUxV1pze8GtKMIOj5QV9ny1KI3y/0EeAqm1rJOHe2BKfhMeZ10HeNn60aye6sa98ThBwEPXsLURWlGtMbdx+e7WcNdq2pGXF0he2LkP4EsxxRi51CHMMctdgv+LXMv5x1fvPr0bU/mt1Mce+aRsVRf317TxwEHUB/R9NaYAtSh9AL9W2zP0Ytboej1c0pRqhFVGNHiGHM8PHqkWPFetDr72xuwWwGHk5neh4Lgl7sdqGrebnsR0BXAgcMNza09Ty7IK69o3mF4KfIFxodjMy/r8/07AmCXqTw366K+geq/0Ho2rzWOWK79Xdn1j+08su/KtspvL+z6VKjBzEtEwkJ220+1pDpORAEvchwpLM5ivDHQQ9zQsEW5w6V/xvElobWnsNlJeRN70w5G18ncTd4lfO71WTBfw3KNGR6hiu9D8NK4eTGWGm4G6i288UMkpYAX5X5Zt+9LWVTWHygY3UdI/F/kXgQvMrOrSwKj9W9Swn9heCyMEIvJt/c/q5IVce3It02IeJ8QU/ig+BWYn6Q3tjbPx/XGz/4bgZ+O/QB4B7B2ukWaIx2VEUjf1q3YXc2CHoxCLqj+VLgRxKnra/ssbdE7CHgRdADyM8eP2Okfy6Wo/s7m6oxLYibja4FNybsxlj4+jjFE41f7a3YKC9YjgncOHqO9oRwp+BjRc+NahCXIR4GHkkNpz7c39FSsqPVDm0+P+rrajrPZgvip7Y+j2kcL6mbbzOm5pHhFmVJhxG60h8G7205O4r8f22qk+4TOsUUf8TwHcH/kNlZn5ndw+PA5ndWx8PxKsHlwPWIy2yime8JIYt9TTrT+1QQdIVioLM5smlD3JPk79vTtJqJJfdjHbK8XeafQb0WR8CDso/JUTaKPHy8SrGOq9pVcUpWtWAJuC63kqj3AJfZnAOsQMWpFCvztMle05B5rSITj1BOF5Yiv3diNDv9CD02Ouf4lPsxIqDR0Ii1GvkzQBZ8ADgA6rcYHIEhjZBFrpWpAWpzlkcrEOkTRlZEon0g03LLa03VeUB3EHQFIjarhdYmnavyLcfJAp+aAymslZJXIo0X3h87g2iiCL8miTjp5xfCBStA6ypV0OGhEL8XUTP+wJf/8DcFH3sGy9UY10LkkfD7QspRgfh/970zQrp0umRjqpQjQapQvhxd8tvN56eCoCsMNUMj52KfU9B47vI7n3AG5xnWpY6nLgqCrriIR+cKpQsUw0K3HGO+/cIg6Mrzz8uRlyw6y5H7Puf78WuCoCtKzuJPbBXkJSvBcuQqI/js/j2vVtyZhos7tjPLZ+A/J03fLFAuSENUDRwLI3TFeGiWJLEZlWg5bNXYqrhDOhe3h5bqCh7UK8RySK6VKm+GXuyWI7VoLYdJWa64AW3RrxQuVsuBKnNnWlj6XqSWwxXaP4t+c9JitRxMtw02jNDBciwkrgrdCb+4F1ZmkMFWiuXAGsZRxb0wu6gth2CPF6vlwPvBFVead7GP0D/DHF6kKceP0yPLgqArCekR/kXyViDx+3UL33IoRvwknT32PbX/vBJn3cWNgc6mOqPPGW4HluuEEmBTPkQuyFZzjDko9DS4I53prchad6GMwSj6OluawGuTiHoBWqsY+7DQy0B3ehHUuAsICAgICAgICAgICAgICAgICAgoGyyaHDpuh76qNakUUU2MaySlDNFoKbms7SxEQ3Gqeujtd70cB2kEQZcdftfeHGUjzkFcIny+YaXQUuMlkmqBFCYeFfQxUD/iiOA123skv5xu27U/yCQIel4w0LmmxuhCw1rMe4FLQMvG39Bw3tsaCTmwT+IF4J8xvzBV+xoyO8MIPld9+rULamPHKbJx3HB372DFC7r/a001HtG5gnXI1wPnYJYhjW68mlSwdhacYcx+xIuGRyRvr5IOn7WhJ4i7mALuaK6JYTVwNfKfCq00RNhI2mfzr+DnFEW7063dQxUj6Df/69ooGnmrRfZXbK0bO+zHuS9OyXju11jol8DDWI+mM92DQYpFGJw6Ws4F34X4KNA41vZIuVrao/1g+4ikJyw2NbT27FvQgo7bz4kGUmdeiblD4mogNVM7MSM+/vto48IB8D2O/UTjxl1Hgixnhr6OputA90ssL2D+PAC+o6Gt94kFJ+g3O1uiCK/Gvh1xrVBj8exEwfbjhJHbQzY7kDYR80LDxrCbLbmQ10RCV1s8kl8JtgAcMdxg67nGTHe8YDb4R/gzwH+X9HmhRudtTJ5Tnj+9je+aV42kywU/QmT6O1ang1STphJRC7AFk3beQFEAb8RsEV61IEbovo7mVRIdwHVzai0SWo6p/DbwIvClhkzvL4JkT9O3XU3nyPpHYG0R5szHVcWny1bQRztbqmPiS402Cy5i/HWx+bIZp7Mckzm5E6sOSNyO9JN0a3ewICc+AHY21dp6WOKDFOFlbcMh4CNlaTn6OltqY/xXoB9iXzwm5nmzGdNajskcxGjqsg37cwfa3x0qVOWb3q6WaqMvgj/qUTEXaDNO4rKXCq8ruxF6oKNpeYy+LvHJcQHNt80o0HJMsh9iGHjI8p2NrbuOLXYxxw++m6OH/nAT0lagpsjz5y/LRtAD7U0o8tlxpG2YK1F+Zf1ysBnJLccUPAt+HNgM7E639S5aC9LX2bwO2CZYWYKPP6Ty+aJN58n6NvLl+cIp+ULJDBZWZtpotvdJ/Bzr1xZDGrc0059gOyuOY8EgeK+gt76td15mioGvNS91zL8aVouTF02KwIfLQtD9nU3rDA9izj1pVWiBW45y40hHBc9hbRXeXj9Hb4Af6WpZJvthwVWlmj+B/nkV9EBXU3Ucc5Wkb0NuheikSi6VYTnKj5vDiMcds6lhY09JdxT2dzXXOmaTxG2UsPyc4eV5ffq2+bjEd8HL8wKCk0Qz78lGgSnHguBiKebziH8YuLdpaWmfBP0Zib8AUrNNM07HBTvmN07KlVlLO084niQiGDvsvWy4mXx9C5mLSOIySz/s72hZXvTnovZ3Rf0dzeuQ7jcsKe1N6qPgf5pXQQu/ILNdow0tiXw+NmBLLh+uiUZ03rUuZI64HHzHm11rimoHlMpeaNgKVE9O6YvPsXZY0S/mVdB1I384CHz71FcZLMcc8ZTlT0VWS/Gej5qXAZslVpfSZoxa16PA1xtauwcT3ZFuv4KBqkOrEJfbeg9wnkSdc1U7Xwf+jxw/D+xMZ3YlfmpW+z7cfs5/G0iduR5omWrv69h0L1EefFTbC/qBcErO2TY3AK/MVsyDXS3Vx2N/U2Ld+GyQdxMVneNN4OeZLlI9em9TKo50DnAr9geRVgLVOac7NmLluO0DQi8gttTVvfVi9Jd7kz8FdzTdZLRVoiakHPMa6fXWn9fUHF37o9mMzNU2fwneZJNSqc++MNvt+D82bNw1CKepDz3Q2Xx2LHVg/0/gy0irGK3KqbyOHeOSViA+ZfinowNv++Dv2i+ICriop4R7g+WYXy5YfXTPniWz0lfsDwCtoFT+zVIay+GDhvVjYj5JPm/89b+L6o8dbTH+NOg241pZo0cZnDwqn8jz7vpj4K8MvS310PL1yV4o7eto/iTy90GpsLAyfzyW1jS2du+e2Wpv82XADwUrJu8+LMk8OWz4UkNbzzfyryGVm/Kbo1g6R8eOfimGDwitBCIVuNQ7Edq7Fqm1ZmhkL/B8wgTvBVk7hC9FmpRyMFo8oyz4CSmHRq+1YrhndrrskY7m5TLfRF6RaydNulGKnWzYfhLx6InXEY3+rSsj+6fAF5V7azoaU5lOsBan55PI2YYOtzcnaqDfn1l3ELEtWI755bEp+L3Iga6mdCTfh2jJl18JLccRiO5qaOvtP0nQ/R3Na4Hvjm0Yye+y/BOinP/fT8EZvSvzTlq6ZKCKT7q9edpG+eMv/4o/ZGt+YOgNCyvzxg++PdNzsBAxH+1oqY5jMkafOtXnF/WmM0exv9CQ6d431fVE4JuBFZOEnDfQFnKEbt60lb+Cc/NA5GVJ/v2K9peyWFsMQ2FhZW75aAc+V9CKdvsVxPgmoZt0uoWb4lmOWPAY6KlTXVNkuBqIJnK9PBMxO8sx9odLiXRF8tXD+Ek8mngEyzFnHDhq6R8LshpVhy6y6ELUne7zi2Y5oNuwKZ3pGTqloCUtH11todiWwzYWKcz6gXubG5M0Ujqz6zDifuNssBxzxw3PxejZAtYOWoBHsNPTfX5xbjoPId3akOnZd7rrigxv+EQhF9FyjPIm516GTPhBeh60I1iOOeJoj6Dr7W3JXuYd6GxahtgscV6ivSKzthwexvztW9XR9umuLcI8S37mW2zLIUDUAnf4wWQvi75VEx0Evhssx5zwQcQXomzVzsTe2XSBrmJiUY1SWg7QDklfT7Kmob6O5guAHwMrcz/fOa8xtpgyg4WV07wmc0M6y6Nq75m20Q7ed0Gq5q2R3xiawsJKKThIbMd8KZ3p2Z7IZnQ1V9u+DXN/IT9rVgsouQKZH0q39byc5BqjyCPdwNMlthxjCy4391clSzyWr9+ZNWwVHgqWo6j8GGYvOGP7+rplNYnEfLh9VUTs9wtlCt6eOnPLESO6bCeePVL1G3fHfR3N25CuNSxTKSzHBC5Fuhx4IuH1PWl0i+DCshnZ8r5SCWzAG0J7MdnxxnNeQ86QC2UNg4K9gt8A2xsyvW8UkmicUXVGi3NvrTcW+r1m/NKreb5qpOo7Z7XvzCZPyYCBjjWpmGiTxF+V2HKA2Q56X0Omuz/ZHo+mP5e0jbmuMjqXlgPtQNxTf17TM5rFTrdSob+raalj/reU2y9d6HecoeXYjfShhtbuPYVcawRQn9mVBT9gGCyl5VAuG7xAcvLEQzzvXK24SrUcvwA+W1euYr63udEx90lqmfEbMYVbjkFBx1vV2lPo9Y6nDg2Z3n04t9mjZJYjN9jXGm53+xWJEo/jWR3AZZR4FDnlsLkrnenpjcpQzH5wLYjPA5+a3XcsLNkAHhM8tXz9q8xY0KN4wNYbRV1YmYJjXzRQ9duPu336BcRl7T28VVP1HXJTUGUtrJgdDZmesq1Q2v/btz6HuEtSajbftyDxw+4oGvlKfVvPjIrhRJP/EHcDT1sltBzjF+9bjlb9NtHr88vX78za3lIWiUcxLYfoLUchH+lcE/V3Nl0k6LK9ZLbftwDLcUiwvm7D7qMzvfZJgq7P7IqBbeRKk5bEcuTdkpdZXF6Al37SsKeSLIdNmR44VHW24QFgaXFsVcIFFPO9quiMZ2dz5dHJuom7MT8opeUY/SLVwPqBrpa6JBfa0NZ7CHQ/jIqgIvZy+GI/fk152YyOlmWR/aDQxcXaK5Joc5T0jCM2n7XhlWxRBZ3OSzxKaTlkg91ixx8o4Ec8Z89z4lFEyyFpdf+e3isOtZ8/7/WjB+5tivo6m88Ff9/kva1djLof00/aR2Tuequ6ataHLk3ZkFMlHsW3HIC0BHSH25NdbJwdyr3VUjkpRwp7c3WqauV8ivnNzpbqWHxK9o8RV0sT9edKbjnMMOa+dKZn5/L1O2f9XXTqBY3cHg/BymIurEzJ0cfSIz1PKoGw32xfVV2Vqn7V6LwK2suxB7PJip+piny4bsPuknprtzdHR6u0zPIq4BLwLaBVRfouBS2sAI9hfTad6R4qxnc7paAHOtZEMdEWxBdFMkHnvSRbUAFj4+fB1ze07TqczOc13Yb0dXBNBdXlOGb8huAIKC7p97CqgTrwUqARKSr1loFTYDdwTbqtp7tYN+spKyfVZ3bl9njAtYzu8Si65Zj4w+WYS4Gnk92GfhK4BXRBqTpgHvZy1AqtPvV4U0Q+RUeVcqvqqWZnzN2pSEWNLqPT/8+8xKPoKcekKqPVgjv7712TKPFIt+06iNkyHnuFN1bKmk8h8tj2Q+lMz+NLWrvjORP0iXs8ip5yTOK6AOn9BZilZyXPfeJRoS/JlrS66clzxMuCTaV4Ppg2LhpLPE44tKy4liP3VssSo9uTJh4jqT+an7daiptyLAp+wouuRw13pzO9++ZF0KOYvMejyJYjr47Hpf1VzR9OIuq3f/WlOE790Xds9gbLsXAsB7BJjp+jREgkaE3xVkvxLcfEHo/+qjWJ3hBv/OpLw+TOiR4OlmNBWI5fIv1dbvFuHgWd3rg7BrYZHSqZ5Zi4m69AujRxpoqeYC73eATLMbOUAw4Cdza0dg9SQiReclWCxGO2lmPUa9Vg1vd1NCcq69qY6T6IuH/OEo9gOWZiOYYN96TbSr9VNrGg0wkSj2JYjtHEYy2QPPEwzwEvB8tRtpbjScxjzAEK2hQzfeJRpNxfLEHcnljPI2ccmLPEI1iOgrjhMNKdDZme/rIT9HSJR3Esx/jS+GX9HS2JduI1tr8SM3LG35s5SDyC5UheXsw+iv2FdGv3G8wRChb06RKP4lmOsTs9vrW/I1ni0dD+yjBmq0qdeATLkZATS3oU/DRziIIFffrEoziWI2/EvxLp4uQ3m0ufeATLkZCrG7O5IbNrqKwFfbrEo5iWY/ROrwHuHEiYeKQ39h4AtpY08QiWIwk/Fpub09NUCi0bQZ8q8Si+5QCstRZXJb86PyP5lWA55oszDPzdH0ZqXmQeMONXf6ZOPIpsOXIzQB0kTzxUFR/A+n6wHPNWOH076L5/0/5SvKAEPVXiUQLLMTbdX9Hf0Xx1kguq37A7jqqy3wJeD5Zjjjnsl/yldFv3YeYJsxL0iYlHKSxH3nT/hb7O5nSS66rb8NoweCv2cLAcc8azQJelncwjZiXokxOPEliOiel+neyLC7i8xyX2BssxFzz37FKVPf73Da092QUr6BMTjxJaDhA1Rnf2dTTXJrrZ2noP4BIkHsFyTMVfwLqlrn3PvIq5KILOTzxKbDkAXwSFJB48I1HcxCNYjhPPZ3kZ85V0gecblq2gJyUeUiktB0BBiUdU9bb9QHETj2A58vkB4RvrRnKDRsUIeiLx4I2SWY6J4fHKvo7mRKN03YZfx1VR9UOYfcFyFJnbLwq/rz7T+0rUXj41J4sm6LHEo5SWY5wreeJx1oZfDwHFSzyC5YiFfgJ89qzssp2UGYom6InEg0OlshxjXLnaaxclvzo/Lng9WI5Z82HD9wT/OZ3p7Y7af07FCjr3YWOJR7Gq/ZwSteD1fZ1rkiUemd79iC0w+/K1YrHCO7FvSL+j5sb6TM+Rcr3Kogo6v46HRyXp0e1LM+MT0j6Jm4tB65L3h57B7DztZybgJlfFbLwIfwXzURwEvgH+T478pG55qaxvu6KXcR2vXGrQqALEzLjyRsUpeBr0haTXlYq0H/H9aT5zWi4bKd9LVyzPAj+w+URVdMYd6bZd+xtad8XlPo+UZAYdr1wqr5yt/Ziu3pzxf2ho630hyXX9rqulZsR+zfbKIhcdrBTE4FcwzxLpu+nWnr0L7QtEpfnQ0bNaXELLMf67bu/rbEpUE++s1u4h7K2jDzfBcpghm4OYlzHfMvwZ4s8iZe9eiGKG01QfnaWXHj+dFnuZRo8C8FjslZCPHYY6zVi+zugiINEoLfSYzTUS507K4ZLyE+Y1LQyelRiyGTQcljhg2GV7L+h1Ynrr3TOYpD73orQccOLptKWzHKN/99ko0sfqNszsKLCAykHJzvYoTuKR2BJcEo/4ytCdASU9rGY08XisRCnHOLdJg24N3RkwB6cvaavN/lwBmYnNS0l4gebp/X1dLReHLg2CLimqiHqBp0CltBw5x21/YrDjXVHo1iDokqEuszPL2B6PElkOJqzHhcfJpkO3BkGX+Ifk9niU1HLk/s1KRF3o1iDokmI88bAHS2k5sGvA1aFbg6BLjobcmRoF7fEofK+FsrKyoVuDoOcKW8H7S2c5fBDFg6Fbg6DnBFWkdoMKSDwKtRzarTjqD90aBD0nGE887EOlsBzAj+s39gyHbg2CnsMfGHdDssSjEBi6f3dm3VOhS4Og5xT1mV1ZoW8m2+ORzHIIHZF9zx9/+Vdx6NIg6DlHOtPzepI9HgktR4z8kKuqng7dGTCPy8TeYp8+8UiAYeAuKbqnYcOrwTsHzJ+gc4nHdHs8TmEzzDDiFcONgr+pb+0eCl0ZMINHr+Kir6P5QsTPcm+1CEYPOR/jY8UfJ59IqsOCzciPpdt694cuDMhHal5/elXUzUi8RVLXuOVgasthswNpm+Lo0fTGV8PiSUD5jdAARzqa6iJpm+ED4JqJyzKgIcFOzDabp010qHHjqyHJCChfQQP0dTansa8D3QBcJDFM7qjjbUbPNszjEQcBCwv/H0HUti0Y9BFdAAAAAElFTkSuQmCC',
    data: {
      type: payload.data.type,
      uuid: payload.data.uuid,
      notificationUuid: payload.data.notificationUuid
    }
  });
  // return self.registration.showNotification(notificationTitle, notificationOptions);
});
// [END background_handler]

self.addEventListener('notificationclick', function(event) {
  event.notification.close();
  var url = ''
  baseUrl = new URL(location).searchParams.get('baseUrl');
  url = baseUrl + '/#/orders/' + event.notification.data.uuid;
  // if (event.notification.data.type === 'Facility') {
  //   url = baseUrl + '/#/facility-booking/' + event.notification.data.uuid;
  // } else if (event.notification.data.type === 'Complain') {
  //   url = baseUrl + '/#/complaint/' + event.notification.data.uuid;
  // } else if (event.notification.data.type === 'Panic' || event.notification.data.type === 'PanicResolve') {
  //   url = baseUrl + '/#/panic-alerts/' + event.notification.data.uuid;
  // } else if (event.notification.data.type === 'Request') {
  //   url = baseUrl + '/#/request/' + event.notification.data.uuid;
  // } else if (event.notification.data.type === 'Task') {
  //   url = baseUrl + '#/tasks/' + event.notification.data.uuid;
  // } else {
  //   url = baseUrl + '/#/visitor/' + event.notification.data.uuid;
  // }
  url += '?notification_uuid=' + event.notification.data.notificationUuid;
  event.waitUntil(
    clients.matchAll({type: 'window'}).then( windowClients => {
        // Check if there is already a window/tab open with the target URL
        for (var i = 0; i < windowClients.length; i++) {
            var client = windowClients[i];
            // If so, just focus it.
            if (client.url === url && 'focus' in client) {
                return client.focus();
            }
        }
        // If not, then open the target URL in a new window/tab.
        if (clients.openWindow) {
            return clients.openWindow(url);
        }
    })
  );
});