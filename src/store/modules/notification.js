import {Notification} from 'vue-notifyjs'
import router from '@/router'

export default {
  actions: {
    welcome ({state}, message) {
      Notification.notify({
        component: {
          template: `<span>Welcome, ` + message + `</span>`
        },
        icon: 'nc-icon nc-satisfied',
        horizontalAlign: 'right',
        verticalAlign: 'bottom',
        type: 'success'
      })
    },
    success ({state}, message) {
      Notification.notify({
        component: {
          template: `<span>` + message + `</span>`
        },
        timeout: 10000,
        icon: 'fas fa-check',
        horizontalAlign: 'right',
        verticalAlign: 'bottom',
        type: 'success'
      })
    },
    firebase ({state}, content) {
      Notification.notify({
        title: content.title,
        message: content.message,
        // component: {
        //   template:`<span><a href="https://www.google.com" target="_blank"> Go to google</a></span>`
        // },
        timeout: 1040000,
        icon: 'nc-icon nc-bell-55',
        horizontalAlign: 'right',
        verticalAlign: 'bottom',
        type: 'info',
        onClick: (a) => {
          if (a.toElement.tagName !== 'BUTTON') {
            if (typeof content.link !== 'undefined' && content.link !== null) {
              return router.push(content.link)
            } else if (typeof content.afterClick !== 'undefined') {
              return content.afterClick()
            }
          }
        }
      })
    },
    error ({state}, message) {
      Notification.notify({
        component: {
          template: `<span>` + window.serializeErrorMessage(message) + `</span>`
        },
        timeout: 20000,
        icon: 'fas fa-exclamation',
        horizontalAlign: 'right',
        verticalAlign: 'bottom',
        type: 'danger'
      })
    },
    clear () {
      Notification.notifications().clear()
    }
  }
}
