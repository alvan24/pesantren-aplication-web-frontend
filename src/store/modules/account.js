import axios from 'axios'
import moment from 'moment-timezone'
// import firebase from '@firebase/app'
import '@firebase/messaging'

const accessTokenStorageName = 'access_token'
const refreshTokenStorageName = 'refresh_token'
const accountDataStorageName = 'account_data'

export default {
  state: {
    httpInterceptor: null,
    responseInterceptor: null,
    accessToken: localStorage.getItem(accessTokenStorageName),
    refreshToken: localStorage.getItem(refreshTokenStorageName),
    accountData: localStorage.getItem(accountDataStorageName),
    isOffline: false
    // firebaseMessaging: null
  },
  getters: {
    tokens: state => {
      return {
        access_token: state.accessToken,
        refresh_token: state.refreshToken
      }
    },
    accessToken: state => {
      return state.accessToken
    },
    refreshToken: state => {
      return state.refreshToken
    },
    accessTokenHeader: state => {
      return 'Bearer ' + state.accessToken
    },
    isLoggedIn: state => {
      return state.accessToken !== null
    },
    accountData: state => {
      return JSON.parse(state.accountData)
    },
    isOffline: state => {
      return state.isOffline
    },
    allowedRoles: state => {
      const a = JSON.parse(state.accountData)
      if (a.role !== undefined) {
        return a.role
      } else {
        return null
      }
    },
    isAllowedRoles: (state, getters) => (keyword) => {
      if (getters.allowedRoles !== null) {
        return getters.allowedRoles === 'admin' || getters.allowedRoles === 'admin-hospital'
      } else {
        return false
      }
    }
    // firebaseMessaging: state => {
    //   return state.firebaseMessaging
    // }
  },
  mutations: {
    setHttpInterceptor (state, data) {
      state.httpInterceptor = {
        request: data.request,
        response: data.response
      }
    },
    login (state, tokens) {
      state.accessToken = tokens.accessToken
      state.refreshToken = tokens.refreshToken
      if (!tokens.account_data !== null) {
        state.accountData = tokens.accountData
      }
    },
    logout (state) {
      state.httpInterceptor = {
        request: null,
        response: null
      }
      state.accessToken = null
      state.refreshToken = null
      state.accountData = null
    },
    checkIsOffline (state, status) {
      state.isOffline = status
    }
  },
  actions: {
    login ({ dispatch }, credentials) {
      return axios.post(window.AppConfig.baseApiUrl + 'oauth/token', {
        username: credentials.username,
        password: credentials.password,
        grant_type: 'password',
        client_id: window.AppConfig.clientId,
        client_secret: window.AppConfig.clientSecret
      })
        .then(response => {
          const a = response.data
          return dispatch('getAccountData', a.access_token).then((response) => {
            console.log(response)
            // if (response) {
            //   return dispatch('saveTokens', {tokens: a, accountData: JSON.stringify(response), isLocalLogin: false})
            // } else {
            //   return Promise.reject('I\'m so sorry, You don\'t have permission to access on this website.')
            // }
            return dispatch('saveTokens', {tokens: a, accountData: JSON.stringify(response), isLocalLogin: false})
          }).catch(error => {
            return Promise.reject(error)
          })
        })
        .catch(error => {
          return Promise.reject(error)
        })
    },
    localLogin () {
      return {
        data: {
          accessToken: localStorage.getItem(accessTokenStorageName),
          refreshToken: localStorage.getItem(refreshTokenStorageName)
        }
      }
    },
    // initFirebase ({
    //   state
    // }) {
    //   if ('serviceWorker' in navigator) {
    //     if (!firebase.apps.length) {
    //       firebase.initializeApp(window.AppConfig.fbase.initializeConfig)
    //       state.firebaseMessaging = firebase.messaging()
    //       state.firebaseMessaging.usePublicVapidKey(window.AppConfig.fbase.vapidKey)
    //       navigator.serviceWorker.register('./static/js/firebase-messaging-sw.js?baseUrl=' + window.AppConfig.webUrl).then((registration) => {
    //         state.firebaseMessaging.useServiceWorker(registration)
    //         state.firebaseMessaging.requestPermission().then(() => {
    //           state.firebaseMessaging.getToken().then((currentToken) => {
    //             if (currentToken) {
    //               return axios.post(window.AppConfig.apiUrl + 'mobilestate', {
    //                 firebaseId: currentToken
    //               }).then(() => {
    //               }).catch(() => {
    //                 window.consoleLog('cannot save notif token')
    //               })
    //             }
    //           })
    //         }).catch(() => {
    //           alert('Please Allow Notification Permission')
    //         })
    //       })
    //     } else {
    //       state.firebaseMessaging.requestPermission().then(() => {
    //         state.firebaseMessaging.getToken().then((currentToken) => {
    //           if (currentToken) {
    //             return axios.post(window.AppConfig.apiUrl + 'mobilestate', {
    //               firebaseId: currentToken
    //             }).then(() => {
    //             }).catch(() => {
    //               window.consoleLog('cannot save notif token')
    //             })
    //           }
    //         })
    //       })
    //     }
    //     state.firebaseMessaging.onTokenRefresh(() => {
    //       state.firebaseMessaging.getToken().then(refreshedToken => {
    //         return axios.post(window.AppConfig.apiUrl + 'mobilestate', {
    //           firebaseId: refreshedToken
    //         }).then(() => {
    //         }).catch(() => {
    //           window.consoleLog('cannot save notif token')
    //         })
    //       })
    //     })
    //   } else {
    //     alert('This browser not supported Notification')
    //   }
    // },
    // clearFirebase ({
    //   state
    // }) {
    //   if ('serviceWorker' in navigator && state.firebaseMessaging !== null) {
    //     state.firebaseMessaging.getToken().then((currentToken) => {
    //       if (currentToken) {
    //         state.firebaseMessaging.deleteToken(currentToken)
    //         state.firebaseMessaging.onMessage = function () {}
    //         // state.firebaseMessaging = null
    //       }
    //     })
    //   }
    // },
    refreshToken ({ state }) {
      return axios.post(window.AppConfig.baseApiUrl + 'oauth/token', {
        grant_type: 'refresh_token',
        client_id: window.AppConfig.clientId,
        client_secret: window.AppConfig.clientSecret,
        refresh_token: state.refreshToken
      }).then(response => {
        return response.data
      })
        .catch(error => {
          return Promise.reject(error)
        })
    },
    logout ({ commit, state, dispatch }) {
      dispatch('notification/clear', null, { root: true })
      // dispatch('clearFirebase')
      localStorage.removeItem(accountDataStorageName)
      return axios.delete(window.AppConfig.baseApiUrl + 'api/v1/oauth/token', {
        token_type_hint: 'access_token',
        token: state.accessToken
      }).then(() => {
        axios.interceptors.request.eject(state.httpInterceptor.request)
        axios.interceptors.response.eject(state.httpInterceptor.response)
        localStorage.removeItem(accessTokenStorageName)
        localStorage.removeItem(refreshTokenStorageName)
        commit('logout')
      })
        .catch(() => {
          localStorage.removeItem(accessTokenStorageName)
          localStorage.removeItem(refreshTokenStorageName)
          commit('logout')
        })
    },
    saveTokens ({ state, commit, getters, dispatch }, params) {
      let a = params.tokens.access_token
      let b = params.tokens.refresh_token
      const c = params.accountData === null ? state.accountData : params.accountData
      if (!params.isLocalLogin) {
        localStorage.setItem(accessTokenStorageName, a)
        localStorage.setItem(refreshTokenStorageName, b)
        localStorage.setItem(accountDataStorageName, c)
      }
      commit('login', {
        accessToken: a,
        refreshToken: b,
        accountData: c
      })
      // set default timezone based on Account Data
      moment.tz.setDefault(getters.accountData.timezone)
      const requestInterceptors = axios.interceptors.request.use(config => {
        dispatch('setIsOffline', false)
        if (state.accessToken !== null) {
          config.headers.Authorization = 'Bearer ' + state.accessToken
        }
        return config
      })
      const responseInterceptors = axios.interceptors.response.use(undefined, function (err) {
        if (err.response === undefined) {
          if (err.message === 'Network Error') dispatch('setIsOffline', true)
        } else {
          dispatch('setIsOffline', false)
          if (err.response.status === 401 && err.config && !err.config.__isRetryRequest) {
            return new Promise(function (resolve, reject) {
              return dispatch('refreshToken').then(response => {
                localStorage.setItem(accessTokenStorageName, response.access_token)
                localStorage.setItem(refreshTokenStorageName, response.refresh_token)
                commit('login', {
                  accessToken: response.access_token,
                  refreshToken: response.refresh_token,
                  accountData: state.accountData
                })
                err.config.__isRetryRequest = true
                // err.config.headers.Authorization = 'Bearer ' + response.access_token
                axios(err.config).then(resolve, reject)
              }).catch(() => {
                dispatch('logout')
                return Promise.reject(null)
              })
            })
          } else {
            return Promise.reject(err)
          }
        }
      })
      commit('setHttpInterceptor', {
        request: requestInterceptors,
        response: responseInterceptors
      })
      if (getters.accountData === null) {
        return Promise.reject('I\'m so sorry, You don\'t have permission to access on this website.')
      }
      // dispatch('initFirebase')
      return responseInterceptors
    },
    setIsOffline (
      {commit, dispatch}, status
    ) {
      commit('checkIsOffline', status)
      if (status) {
        dispatch('notification/error', 'Please check your internet connection.', { root: true })
      }
    },
    getAccountData ({ state }, accessToken) {
      return axios.get(window.AppConfig.baseApiUrl + 'api/v1/me', {headers: { Authorization: 'Bearer ' + accessToken }})
        .then(response => {
          return response.data
        })
        .catch(error => {
          return Promise.reject(error)
        })
    }
  }
}
