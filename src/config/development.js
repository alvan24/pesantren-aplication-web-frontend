import configInit from './init'

window.AppConfig = {
  // ://localhost:8000
  subDomain: configInit.subDomain,
  webUrl: process.env.IS_BUILD_DEV ? 'http://localhost:8000/' : 'http://localhost:5000',
  baseApiUrl: 'http://localhost:8000/',
  apiUrl: 'http://localhost:8000/api/v1/',
  webSocketUrl: 'ws://localhost:8000/ws',
  clientId: '1',
  clientSecret: '07CeFTnT5pD2w2bqhNqUS7PqxpDIqThJpJ04u8d6',
  googleApiKey: 'AIzaSyDUV2cp-xYLzP67rob5YF-mVSyWNnTtG40',
  fbase: {
    initializeConfig: {
      apiKey: 'AIzaSyDCZaRs9WkbYviIr1yRA3ay4JqOjiCU9ss',
      authDomain: 'genie-dev-218716.firebaseapp.com',
      databaseURL: 'https://genie-dev-218716.firebaseio.com',
      projectId: 'genie-dev-218716',
      storageBucket: 'genie-dev-218716.appspot.com',
      messagingSenderId: '282322269583'
    },
    vapidKey: 'BOClVtO2xviM4Kg_IrIRPIizUaY4pjdB-355wUmQCgjjmEc3X9R5TxGOmKv2_gkRujrk3vEXkS1gi1uxt9StCFg'
  },
  IntervalRefreshData: 120000,
  IntervalRefreshDataDashboard: 300000,
  dateFormat: 'DD-MM-YYYY hh:mm:ss A',
  shortDateFormat: 'DD-MM-YYYY'
}
