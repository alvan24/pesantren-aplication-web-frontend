let URLfull = window.location.host
let URLparts = URLfull.split('.')
let subDomain = URLparts[0].indexOf('localhost') === -1 ? URLparts[0] : 'dev'
// let subDomain = URLparts[0].indexOf('localhost') === -1 ? URLparts[0] : 'cemaraasri'
let Init = () => {
  return {
    subDomain: subDomain
  }
}
export default new Init()
