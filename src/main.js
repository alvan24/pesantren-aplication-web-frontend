import Vue from 'vue'
import router from '@/router'
import store from '@/store'
import LightBootstrap from './light-bootstrap-main'
import App from './App.vue'

try {
  require('./config/' + process.env.NODE_ENV)
  require('./functions')
  if (store.getters['account/isLoggedIn']) {
    store.dispatch('account/saveTokens', {tokens: store.getters['account/tokens'], accountData: null, isLocalLogin: true}).catch(error => {
      if (error === 'I\'m so sorry, You don\'t have permission to access on this website.') {
        store.dispatch('notification/error', error)
        router.push('logout')
      }
    })
  }
  Vue.use(LightBootstrap)
  /* eslint-disable no-new */
  new Vue({
    el: '#app',
    render: h => h(App),
    router,
    store
  })
} catch (ex) {
  alert('Can\'t find config file !!!')
}
