// Notifications plugin. Used on Notifications page
import Notify from 'vue-notifyjs'
// Validation plugin used to validate forms
import VeeValidate from 'vee-validate'
// A plugin file where you could register global components used across the app
import GlobalComponents from './globalComponents'
// A plugin file where you could register global directives
import GlobalDirectives from './globalDirectives'
// Sidebar on the right. Used as a local plugin in DashboardLayout.vue
import SideBar from './components/SidebarPlugin'
// Tabs plugin. Used on Panels page.
import VueTabs from 'vue-nav-tabs'
// Font Awesome 5
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSpinner, faLayerGroup, faShoppingBasket, faShoppingCart, faUsers, faTags, faBars, faEllipsisV, faSortUp, faSort, faSortDown, faSearch, faInfo, faTrash, faPencilAlt, faSyncAlt, faPlus, faPlusCircle, faTimes, faList, faStar, faCheck, faChevronLeft, faSave, faStarHalfAlt, faPrint, faClock, faChevronDown, faHospital, faCalendarCheck, faBook, faBullhorn, faSchool, faCalendarAlt, faHome } from '@fortawesome/free-solid-svg-icons'
import { faStar as farStar, faClock as farClock } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
// Axios
import axios from 'axios'
// Sweet Alert
import swal from 'sweetalert2'
// smooth scroll
// import vueSmoothScroll from 'vue-smoothscroll'

library.add(faSpinner, faLayerGroup, faShoppingBasket, faShoppingCart, faUsers, faTags, faBars, faEllipsisV, faSort, faSortUp, faSortDown, faSearch, faInfo, faPencilAlt, faTrash, faSyncAlt, faPlus, faPlusCircle, faTimes, faList, faStar, faCheck, faChevronLeft, faPencilAlt, faSave, faStarHalfAlt, farStar, faPrint, faClock, farClock, faChevronDown, faHospital, faCalendarCheck, faBook, faBullhorn, faSchool, faCalendarAlt, faHome)

// element ui language configuration
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
locale.use(lang)

// bootstrap
import BootstrapVue from 'bootstrap-vue'

// asset imports
import 'bootstrap/dist/css/bootstrap.css'
import 'vue-nav-tabs/themes/vue-tabs.scss'
import 'vue-notifyjs/themes/default.scss'
import './assets/sass/light-bootstrap-dashboard.scss'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import './assets/css/demo.css'

// library auto imports
import 'es6-promise/auto'

import moment from 'moment-timezone'

export default {
  async install (Vue) {
    Vue.use(GlobalComponents)
    Vue.use(GlobalDirectives)
    Vue.use(SideBar)
    Vue.use(Notify)
    Vue.use(VueTabs)
    Vue.use(VeeValidate)
    // Vue.use(vueSmoothScroll)
    Vue.use(BootstrapVue)
    Vue.component('font-awesome-icon', FontAwesomeIcon)
    Vue.prototype.$moment = moment
    Vue.prototype.$http = axios
    Vue.prototype.$swal = swal
    Vue.prototype.$formatCurrency = function (localeCode, number) {
      return new Intl.NumberFormat(localeCode.replace('_', '-'), {minimumFractionDigits: 2}).format(number)
    }
  }
}
