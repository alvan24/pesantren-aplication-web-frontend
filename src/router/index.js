import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

import EmptyContent from '@/pages/Dashboard/Layout/EmptyContent'
import NotFound from '@/pages/GeneralViews/NotFoundPage'
import DashboardLayout from '@/pages/Dashboard/Layout/DashboardLayout'
// Authehtification pages
import Login from '@/pages/Dashboard/Pages/Auth/Login'
import Logout from '@/pages/Dashboard/Pages/Auth/Logout'
import ForgotPassword from '@/pages/Dashboard/Pages/Auth/ForgotPassword'
import ResetPassword from '@/pages/Dashboard/Pages/Auth/ResetPassword'
// Dashboard page
import Dashboard from '@/pages/Dashboard/Pages/Dashboard'

// Profile pages
import Overview from '@/pages/Dashboard/Pages/Profile/Overview'
import ChangePassword from '@/pages/Dashboard/Pages/Profile/ChangePassword'

import UserIndex from '@/pages/Dashboard/Pages/Settings/Users/Index'

// User Admin pages
import SettingAdminUserIndex from '@/pages/Dashboard/Pages/Settings/Users/Admin/Index'
import SettingAdminUserForm from '@/pages/Dashboard/Pages/Settings/Users/Admin/Form'
import SettingAdminUserShow from '@/pages/Dashboard/Pages/Settings/Users/Admin/Show'

// User KTU pages
import SettingKtuUserIndex from '@/pages/Dashboard/Pages/Settings/Users/Ktu/Index'
import SettingKtuUserForm from '@/pages/Dashboard/Pages/Settings/Users/Ktu/Form'
import SettingKtuUserShow from '@/pages/Dashboard/Pages/Settings/Users/Ktu/Show'

// User Teacher pages
import SettingTeacherUserIndex from '@/pages/Dashboard/Pages/Settings/Users/Teacher/Index'
import SettingTeacherUserForm from '@/pages/Dashboard/Pages/Settings/Users/Teacher/Form'
import SettingTeacherUserShow from '@/pages/Dashboard/Pages/Settings/Users/Teacher/Show'

// User Finance pages
import SettingFinanceUserIndex from '@/pages/Dashboard/Pages/Settings/Users/Finance/Index'
import SettingFinanceUserForm from '@/pages/Dashboard/Pages/Settings/Users/Finance/Form'
import SettingFinanceUserShow from '@/pages/Dashboard/Pages/Settings/Users/Finance/Show'

// User Student pages
import SettingStudentUserIndex from '@/pages/Dashboard/Pages/Settings/Users/Student/Index'
import SettingStudentUserForm from '@/pages/Dashboard/Pages/Settings/Users/Student/Form'
import SettingStudentUserShow from '@/pages/Dashboard/Pages/Settings/Users/Student/Show'

// User Parent pages
import SettingParentUserIndex from '@/pages/Dashboard/Pages/Settings/Users/Parent/Index'
import SettingParentUserForm from '@/pages/Dashboard/Pages/Settings/Users/Parent/Form'
import SettingParentUserShow from '@/pages/Dashboard/Pages/Settings/Users/Parent/Show'

// Student Management pages
import StudentManagementIndex from '@/pages/Dashboard/Pages/Settings/StudentManagement/Index'
import StudentManagementForm from '@/pages/Dashboard/Pages/Settings/StudentManagement/Form'
import StudentManagementShow from '@/pages/Dashboard/Pages/Settings/StudentManagement/Show'

// Broadcast pages
import BroadcastIndex from '@/pages/Dashboard/Pages/Settings/Broadcast/Index'
import BroadcastForm from '@/pages/Dashboard/Pages/Settings/Broadcast/Form'
import BroadcastShow from '@/pages/Dashboard/Pages/Settings/Broadcast/Show'

// Course pages
import CourseIndex from '@/pages/Dashboard/Pages/Settings/Course/Index'

// Academic Calender pages
import AcademicCalendarIndex from '@/pages/Dashboard/Pages/Settings/AcademicCalendar/Index'

// Academic Calender pages
import ClassRoomIndex from '@/pages/Dashboard/Pages/Settings/ClassRoom/Index'

const routes = [
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: Dashboard
      }, {
        path: 'profile',
        name: 'Profile Empty Container',
        component: EmptyContent,
        children: [
          {
            path: 'index',
            alias: '/',
            name: 'Profile',
            component: Overview
          }, {
            path: 'change-password',
            name: 'Change Password',
            component: ChangePassword
          }
        ]
      }
    ],
    meta: { requiresAuth: true }
  }, {
    path: '/settings',
    component: DashboardLayout,
    redirect: '/settings/service-providers',
    children: [
      {
        path: 'users',
        component: UserIndex,
        redirect: '/settings/users/admin',
        children: [
          {
            path: 'admin',
            name: 'Admin User Setting Empty Container',
            component: EmptyContent,
            children: [
              {
                path: 'index',
                alias: '/',
                name: 'Daftar Admin',
                component: SettingAdminUserIndex
              }, {
                path: 'create',
                name: 'Tambah Data Admin',
                component: SettingAdminUserForm,
                meta: { requiredAllowedRole: 'admin' }
              }, {
                path: ':id/edit',
                name: 'Perbarui Data Admin',
                component: SettingAdminUserForm,
                meta: { requiredAllowedRole: 'admin' }
              }, {
                path: ':id',
                name: 'Detail Admin',
                component: SettingAdminUserShow,
                meta: { requiredAllowedRole: 'admin' }
              }
            ]
          },
          {
            path: 'ktu',
            name: 'Ktu User Setting Empty Container',
            component: EmptyContent,
            children: [
              {
                path: 'index',
                alias: '/',
                name: 'Daftar KTU',
                component: SettingKtuUserIndex
              }, {
                path: 'create',
                name: 'Tambha Data KTU',
                component: SettingKtuUserForm,
                meta: { requiredAllowedRole: 'admin' }
              }, {
                path: ':id/edit',
                name: 'Perbarui Data KTU',
                component: SettingKtuUserForm,
                meta: { requiredAllowedRole: 'admin' }
              }, {
                path: ':id',
                name: 'Detail KTU',
                component: SettingKtuUserShow,
                meta: { requiredAllowedRole: 'admin' }
              }
            ]
          },
          {
            path: 'guru',
            name: 'Teacher User Setting Empty Container',
            component: EmptyContent,
            children: [
              {
                path: 'index',
                alias: '/',
                name: 'Teacher Users',
                component: SettingTeacherUserIndex
              }, {
                path: 'create',
                name: 'Tambah Data Guru',
                component: SettingTeacherUserForm,
                meta: { requiredAllowedRole: 'admin' }
              }, {
                path: ':id/edit',
                name: 'Perbarui Data Guru',
                component: SettingTeacherUserForm,
                meta: { requiredAllowedRole: 'admin' }
              }, {
                path: ':id',
                name: 'Daftar Guru',
                component: SettingTeacherUserShow,
                meta: { requiredAllowedRole: 'admin' }
              }
            ]
          },
          {
            path: 'bendahara',
            name: 'Finance User Setting Empty Container',
            component: EmptyContent,
            children: [
              {
                path: 'index',
                alias: '/',
                name: 'Daftar Bendahara',
                component: SettingFinanceUserIndex
              }, {
                path: 'create',
                name: 'Tambah Data Bendahara',
                component: SettingFinanceUserForm,
                meta: { requiredAllowedRole: 'admin' }
              }, {
                path: ':id/edit',
                name: 'Perbarui Data Bendahara',
                component: SettingFinanceUserForm,
                meta: { requiredAllowedRole: 'admin' }
              }, {
                path: ':id',
                name: 'Detail Bendahara',
                component: SettingFinanceUserShow,
                meta: { requiredAllowedRole: 'admin' }
              }
            ]
          },
          {
            path: 'siswa',
            name: 'Student User Setting Empty Container',
            component: EmptyContent,
            children: [
              {
                path: 'index',
                alias: '/',
                name: 'Daftar Siswa',
                component: SettingStudentUserIndex
              }, {
                path: 'create',
                name: 'Tambah Data Siswa',
                component: SettingStudentUserForm,
                meta: { requiredAllowedRole: 'admin' }
              }, {
                path: ':id/edit',
                name: 'Perbarui Data Siswa',
                component: SettingStudentUserForm,
                meta: { requiredAllowedRole: 'admin' }
              }, {
                path: ':id',
                name: 'Detail Siswa',
                component: SettingStudentUserShow,
                meta: { requiredAllowedRole: 'admin' }
              }
            ]
          },
          {
            path: 'orang-tua',
            name: 'Parent User Setting Empty Container',
            component: EmptyContent,
            children: [
              {
                path: 'index',
                alias: '/',
                name: 'Daftar Orang Tua',
                component: SettingParentUserIndex
              }, {
                path: 'create',
                name: 'Tambah Data Orang Tua',
                component: SettingParentUserForm,
                meta: { requiredAllowedRole: 'admin' }
              }, {
                path: ':id/edit',
                name: 'Perbarui Data Orang Tua',
                component: SettingParentUserForm,
                meta: { requiredAllowedRole: 'admin' }
              }, {
                path: ':id',
                name: 'Detail Orant Tua',
                component: SettingParentUserShow,
                meta: { requiredAllowedRole: 'admin' }
              }
            ]
          }
        ]
      },
      {
        path: 'kesiswaan',
        name: 'Student Management User Setting Empty Container',
        component: EmptyContent,
        children: [
          {
            path: 'index',
            alias: '/',
            name: 'Daftar Kesiswaan',
            component: StudentManagementIndex
          }, {
            path: 'create',
            name: 'Tambah Berita Kesiswaan',
            component: StudentManagementForm
          }, {
            path: ':id/edit',
            name: 'Perbaharui Berita Kesiswaan',
            component: StudentManagementForm
          }, {
            path: ':id',
            name: 'Detail Kesiswaan',
            component: StudentManagementShow
          }
        ]
      },
      {
        path: 'broadcast',
        name: 'Broadcast Setting Empty Container',
        component: EmptyContent,
        children: [
          {
            path: 'index',
            alias: '/',
            name: 'Daftar Broadcast',
            component: BroadcastIndex
          }, {
            path: 'create',
            name: 'Tambah Berita Broadcast',
            component: BroadcastForm
          }, {
            path: ':id/edit',
            name: 'Perbarui Berita Broadcast',
            component: BroadcastForm
          }, {
            path: ':id',
            name: 'Detail Broadcast',
            component: BroadcastShow
          }
        ]
      },
      {
        path: 'mata-pelajaran',
        name: 'Course Setting Empty Container',
        component: EmptyContent,
        children: [
          {
            path: 'index',
            alias: '/',
            name: 'Daftar Mata Pelajaran',
            component: CourseIndex
          }
        ]
      },
      {
        path: 'kalender-akademik',
        name: 'Academic Calendar Setting Empty Container',
        component: EmptyContent,
        children: [
          {
            path: 'index',
            alias: '/',
            name: 'Daftar Kalender Akademik',
            component: AcademicCalendarIndex
          }
        ]
      },
      {
        path: 'ruang-kelas',
        name: 'Class Room Setting Empty Container',
        component: EmptyContent,
        children: [
          {
            path: 'index',
            alias: '/',
            name: 'Daftar Ruang Kelas',
            component: ClassRoomIndex
          }
        ]
      }
    ],
    meta: { requiresAuth: true }
  }, {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { requiresGuest: true }
  }, {
    path: '/logout',
    name: 'Logout',
    component: Logout,
    meta: { requiresAuth: true }
  }, {
    path: '/forgot-password',
    name: 'Forgot Password',
    component: ForgotPassword,
    meta: { requiresGuest: true }
  }, {
    path: '/reset-password/:uuid',
    name: 'Reset Password',
    component: ResetPassword,
    meta: { requiresGuest: true }
  }, {
    path: '*', component: NotFound
  }
]

Vue.use(VueRouter)
const router = new VueRouter({
  routes, // short for router: router
  linkActiveClass: 'active'
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.getters['account/isLoggedIn']) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    } // else if (typeof to.meta.requiredAllowedRole !== 'undefined' && to.meta.requiredAllowedRole !== store.getters['account/accountData'].role) {
    //   next({
    //     path: '*'
    //   })
    // }
  } else if (to.matched.some(record => record.meta.requiresGuest)) {
    if (store.getters['account/isLoggedIn']) {
      next({
        path: '/',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
