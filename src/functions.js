window.serializeErrorMessage = function (error) {
  // window.consoleLog(error)
  // let messages = 'Sorry for the inconvenience.<br/>'
  let messages = ''
  if (typeof error.response !== 'undefined') {
    let validationMessages = null
    let subValidationMessages = null
    if (error.response.data.validation_messages !== undefined) {
      validationMessages = error.response.data.validation_messages
      if (validationMessages !== undefined) {
        if (Object.keys(validationMessages).length === 1) {
          for (let i = 0; i < Object.keys(validationMessages).length; i++) {
            subValidationMessages = validationMessages[Object.keys(validationMessages)[i]]
            for (let j = 0; j < Object.keys(subValidationMessages).length; j++) {
              return subValidationMessages[Object.keys(subValidationMessages)[j]]
            }
          }
        } else if (Object.keys(validationMessages).length > 1) {
          messages += '<ul>'
          for (let i = 0; i < Object.keys(validationMessages).length; i++) {
            subValidationMessages = validationMessages[Object.keys(validationMessages)[i]]
            for (let j = 0; j < Object.keys(subValidationMessages).length; j++) {
              messages +=
                '<li>' +
                  subValidationMessages[Object.keys(subValidationMessages)[j]] +
                '</li>'
            }
          }
          messages += '</ul>'
        }
      }
    } else if (error.response.data.message !== undefined) {
      messages += error.response.data.message + '. '
    } else if (error.response.data.detail !== undefined) {
      messages += error.response.data.detail + '. '
    } else {
      messages += error.response.data.status + '. ' + window.ConvertSnakeCaseMesssage(error.response.data.title)
    }
  } else if (error.message === 'Network Error') {
    messages = '<b>You are not connected to internet</b>. Please check your connection'
  } else if (typeof error !== 'undefined') {
    messages += error
  } else {
    messages += 'Please call Administrator to get advance information'
  }
  return messages
}

window.consoleLog = function (value) {
  if (process.env.NODE_ENV === 'development') {
    return console.log(value)
  }
}

window.goBack = function (self) {
  if (self.$route.query.redirect !== undefined) {
    self.$router.push(self.$route.query.redirect)
  } else {
    window.history.back()
  }
}

window.orderStatusColor = function (orderStatus) {
  switch (orderStatus) {
    case 'New':
      return 'secondary'
    case 'Complete':
      return 'secondary'
    case 'Canceled':
      return 'danger'
    case 0:
      return 'secondary'
    case 'Done':
      return 'success'
    case 'Check In':
      return 'success'
    case 1:
      return 'success'
    case 'Accepted':
      return 'info'
    case 'On The Way':
      return 'primary'
    case 'Booking':
      return 'primary'
    default:
      return 'warning'
  }
}

window.ConvertSnakeCaseMesssage = function (str) {
  if ((str === null) || (str === '')) return false
  else str = str.toString()

  return str
    .replace('_', ' ')
    .replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase() })
}
