#!/usr/bin/env bash

sed -i "s/clientId: '.*',/clientId: '$CLIENTID',/" $CONFIGPRODFILE
sed -i "s/clientSecret: '.*',/clientSecret: '$CLIENTSECRET',/" $CONFIGPRODFILE
sed -i "s/googleApiKey: '.*',/googleApiKey: '$GOOGLEAPIKEY',/" $CONFIGPRODFILE
sed -i "s/vapidKey: '.*'/vapidKey: '$VAPIDKEY'/" $CONFIGPRODFILE
sed -i "s/apiKey: '.*'/apiKey: '$APIKEY'/" $CONFIGPRODFILE
sed -i "s/authDomain: '.*'/authDomain: '$AUTHDOMAIN'/" $CONFIGPRODFILE
sed -i "s/databaseURL: '.*'/databaseURL: '$DATABASEURL'/" $CONFIGPRODFILE
sed -i "s/projectId: '.*'/projectId: '$PROJECTID'/" $CONFIGPRODFILE
sed -i "s/storageBucket: '.*'/storageBucket: '$STORAGEBUCKET'/" $CONFIGPRODFILE
sed -i "s/messagingSenderId: '.*'/messagingSenderId: '$MESSAGINGSENDERID'/" $CONFIGPRODFILE
